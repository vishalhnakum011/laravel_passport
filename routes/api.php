<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
// Route::post('v1/register', function(){
// return "ASda";
// });
Route::post('v1/register', 'Api\v1\Auth\AuthController@register');
Route::post('v1/login', 'Api\v1\Auth\AuthController@login');
   
// Route::middleware('auth:api')->group( function () {
//     Route::resource('products', 'API\ProductController');
// });