<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name"=>["required"],
            "email"=>["required","unique:users"],
            "password"=>["required"],
            "c_password"=>["required"],
        ];
    }

    public function messages()
    {
        return [
            "name.required"=>"User fullname must be required.",
            "email.required"=>"User email must be required.",
            "email.unique"=>"User email must be unique.",
            "password.required"=>"User password must be required.",
            "c_password"=>"User confirm password must be required.",
        ];
    }
    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json(["message"=>$validator->errors()->first()], 422));
    }
}
