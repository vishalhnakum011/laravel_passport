<?php

namespace App\Http\Controllers\Api\v1\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\BaseApiController;
use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Http\Requests\RegisterRequest;

class AuthController extends BaseApiController
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(RegisterRequest $request)
    {
        try{
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] =  $user->createToken('MyApp')->accessToken;
        $success['user'] =  $user;
        return $this->sendResponse($success, 'User login successfully.');
        }catch(Exception $exception){
            return $this->sendError('server.', ['error'=>$exception->getMessage()]);
        }
    }
   
    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('laravel')->accessToken; 
            $success['user'] =  $user;
            return $this->sendResponse($success, 'User login successfully.');
        } 
        else{ 
            return $this->sendErrorl('Unauthorised.', ['error'=>'Unauthorised']);
        } 
    }
}
